const express = require("express");
const cors = require("cors");
const mongoose = require("./DB/DbConnector");

//const CONNECTION_URI = process.env.DB_CONN;
const SERVER_PORT = process.env.SERVER_PORT;

const weaponsRouter = require("./routes/weapon.route");
const attachmentsRouter = require("./routes/attachments.route");
const weaponsTypeRouter = require("./routes/weaponType.route");

const app = express();
app.use(cors());
app.use(express.json());

app.use("/weapons", weaponsRouter);
app.use("/attachments", attachmentsRouter);
app.use("/weaponTypes", weaponsTypeRouter);

//Connexion à la bd
/*
mongoose.connect(CONNECTION_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
*/
mongoose.connection.on("connected", () => {
    console.log("Connection to database successfull !");
});

app.listen(SERVER_PORT, () => {
    console.log(`Web server running on port ${SERVER_PORT} !`);
});
