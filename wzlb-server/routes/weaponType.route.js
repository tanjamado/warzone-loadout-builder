const express = require("express");
const router = express.Router();
const weaponTypeModel = require("../model/weaponType.model");

router.get("/", (req, res) => {
    res.json("Une requête GET pour les weaponsTypes !");
});

module.exports = router;
