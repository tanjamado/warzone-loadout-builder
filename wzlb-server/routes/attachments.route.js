const express = require("express");
const { model } = require("mongoose");
const router = express.Router();
const attachmentModel = require("../model/attachment.model");

router.get("/", (req, res) => {
    res.json("Une requête GET pour les attachments !");
});

module.exports = router;
