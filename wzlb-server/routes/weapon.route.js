const express = require("express");
const router = express.Router();
const weaponModel = require("../model/weapon.model");

router.get("/", (req, res) => {
    res.json("Une requête GET pour les weapons !");
});

module.exports = router;
