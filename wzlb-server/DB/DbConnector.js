const mongoose = require("mongoose");
require("dotenv").config();
const CONNECTION_URI = process.env.DB_CONN;

mongoose.connect(CONNECTION_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

module.exports = mongoose;
